import React, { Component } from "react";
import todosList from "./todos.json";

class App extends Component {
  state = {
    todos: todosList,  
  };

  handleAddTodo = event => {
    if(event.key === "Enter") {
      let count = this.state.todos.length +1
      const newTodos = this.state.todos.concat({
        userId: 1,
        id: {count}, 
        title: `${event.target.value}`,
        completed: false
      }); 
      this.setState({ todos: newTodos });
      event.target.value = "";
    }
  }

  handleToggleComplete = (todoIdToComplete, event) => {
    const newTodos = this.state.todos.slice();
    const copyNewTodos = newTodos.map(todo => {
      if(todo.id === todoIdToComplete) {
        todo.completed = !todo.completed
      }
      return todo
    })

    this.setState({ todos: copyNewTodos });
  }

  handleDeleteTodo = (todoIdToDelete, event) => {  
    const newTodos = this.state.todos.slice();
    const copyNewTodos = newTodos.filter(todo => 
      todo.id !== todoIdToDelete
    )
    this.setState({ todos: copyNewTodos });
  }

  handleDeleteCompleted = event => {
    const newTodos = this.state.todos.filter(todo =>
     todo.completed === false
    )
    this.setState({ todos: newTodos});
  }

  render() {
    return (  
      <section className="todoapp">
        <header className="header">
          <h1>TODOS</h1>
          <input className="new-todo" placeholder="What needs to be done?" autoFocus onKeyPress={this.handleAddTodo}/>
        </header>
        <TodoList todos={this.state.todos} 
        handleToggleComplete={this.handleToggleComplete}
        handleDeleteTodo={this.handleDeleteTodo} />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button className="clear-completed" onClick={this.handleDeleteCompleted}>Clear completed></button> 
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input className="toggle" type="checkbox" checked={this.props.completed}
          onChange = {event => this.props.handleToggleComplete(this.props.id, event)} />
           <label>{this.props.title}</label>
          <button className="destroy" onClick={event => this.props.handleDeleteTodo(this.props.id, event)} />
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem title={todo.title} id={todo.id} completed={todo.completed} 
            handleToggleComplete = {this.props.handleToggleComplete}
            handleDeleteTodo={this.props.handleDeleteTodo} />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
